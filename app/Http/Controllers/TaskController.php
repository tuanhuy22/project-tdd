<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    /**
     * @param $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = Task::paginate(10);
        $countTask = Task::count();

        return view('tasks.index', compact('tasks', 'countTask'));
    }

    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());

        return redirect()->route('tasks.index')->with('success', 'Thêm task thành công ');
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function edit($id)
    {
        $task = Task::find($id);

        if (!$task) {
            return redirect()->route('tasks.index')->with('error', 'Task not found');
        }

        return view('tasks.edit', compact('task'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        if (!$task) {
            return redirect()->route('tasks.index')->with('error', 'Task not found');
        }

        // Perform validation and update logic here
        // ...

        $task->update([
            'name' => $request->input('name'),
            'context' => $request->input('context'),
        ]);

        // Redirect to the index view after update
        return redirect()->route('tasks.index')->with('success', 'Update task thành công');
    }

    public function destroy($id)
    {
        $task = Task::find($id);

        if (!$task) {
            return redirect()->route('tasks.index')->with('error', 'Task not found');
        }

        $task->delete();

        return redirect()->route('tasks.index')->with('success', 'Xóa task thành công ');
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');

        // Sử dụng scopeSearch để tìm kiếm
        $tasks = Task::search($keyword)->paginate(10);

        return view('tasks.index', compact('tasks', 'keyword'));
    }
}
