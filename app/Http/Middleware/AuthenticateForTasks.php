<?php

// app/Http/Middleware/AuthenticateForTasks.php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateForTasks
{
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login')->with('status', 'Bạn cần đăng nhập để tạo ,sửa , xóa task.');
        }

        return $next($request);
    }
}

