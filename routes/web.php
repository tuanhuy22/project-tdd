<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('tasks.index')->group(function(){
    Route::get('/', [TaskController::class,'index']);
    Route::get('tasks',[TaskController::class,'index']);
});

Route::group(['prefix' => 'tasks', 'middleware' => ['auth.tasks'], 'as' => 'tasks.'], function () {
    Route::post('/', [TaskController::class, 'store'])->name('store');
    Route::get('/{task}/edit', [TaskController::class, 'edit'])->name('edit');
    Route::put('/{task}', [TaskController::class, 'update'])->name('update');
    Route::delete('/{task}', [TaskController::class, 'destroy'])->name('destroy');
    Route::get('/create', [TaskController::class, 'create'])->name('create');
});


Auth::routes();
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('tasks/search', [TaskController::class, 'search'])->name('tasks.search');

