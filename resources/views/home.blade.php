@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Trang chủ ') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Bạn đã đăng nhập!') }}
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button class="btn btn-primary" type="submit">Đăng Xuất</button>
                    </form>
                </div>
                <div class="card-body">
                    <a href="{{route('tasks.index')}}"  class="btn btn-success"> List Task </a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
