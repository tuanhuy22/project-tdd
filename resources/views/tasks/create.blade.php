@extends('layout.app')

@section('content')
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-4">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="btn btn-danger">Đăng Xuất</button>
                </form>
            </div>
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2>Tạo task</h2>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('tasks.store') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label for="name" class="form-label">Tên</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter task name">
                                @error('name')
                                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="context" class="form-label">Nội dung</label>
                                <input type="text" class="form-control" name="context" placeholder="Enter task context">
                            </div>

                            <button type="submit" class="btn btn-success">Tạo</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
