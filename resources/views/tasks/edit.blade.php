@extends('layout.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Sửa </div>
                    <div class="card-body">
                        <form action="{{ route('tasks.update', ['task' => $task->id]) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Tên:</label>
                                <input type="text" name="name" class="form-control" value="{{ $task->name }}">
                            </div>

                            <div class="form-group">
                                <label for="context">Nội dung:</label>
                                <textarea name="context" class="form-control">{{ $task->context }}</textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Cập nhật Task</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
