
 @extends(('layout.app'))

@section('content')


    <div class="container">
        <div class="logout" >
            @if(auth()->check()) {{-- Check if the user is authenticated --}}
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class= "btn btn-danger" type="submit">Đăng Xuất</button>
            </form>
            @else
                <a class= "btn btn-primary" href="{{ route('login') }}">Đăng Nhập</a>
            @endif
        </div>
        <div class="create" >
            <a class= "btn btn-primary" href="{{ route('tasks.create') }}">Tạo task</a>
        </div>

        <form action="{{ route('tasks.search') }}" method="GET">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Tìm kiếm..." name="keyword">
                <div class="input-group-append">
                    <button class="btn btn-primary " type="submit">Tìm kiếm</button>
                </div>
            </div>
        </form>

        <!-- Hiển thị kết quả tìm kiếm (nếu có) -->
        @if(isset($keyword))
            <div class="alert alert-info" role="alert">
                Kết quả tìm kiếm cho "{{ $keyword }}":
            </div>
        @endif
        <div>
            <p> Tổng số task là : {{ $countTask }}</p>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped">
                @if (session('success'))
                     <div class="alert alert-success">
                         {{session('success')}}
                     </div>
                @endif
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Nội dung</th>
                    <th></th> <!-- Thêm cột Actions -->
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->context }}</td>
                        <td>
                            <!-- Thêm nút sửa -->
                            <a href="{{ route('tasks.edit', ['task' => $task->id]) }}" class="btn btn-primary">Sửa</a>
                            <div style="margin-top:5px;">
                            <form action="{{ route('tasks.destroy', ['task' => $task->id]) }}" method="POST" style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="btn btn-danger" onclick="confirmDelete('{{ $task->id }}')">Xóa</button>
                            </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="d-flex justify-content-center mt-4">
        {{ $tasks->links('pagination::bootstrap-4') }}
    </div>
@endsection
