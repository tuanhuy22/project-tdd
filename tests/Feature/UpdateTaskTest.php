<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    use  WithFaker;

    public function test_update_method_updates_task_and_redirects()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $task = Task::factory()->create();

        $response = $this->put(route('tasks.update', ['task' => $task->id]), [
            'name' => 'Updated Task',
            'context' => 'Updated Context',
        ]);

        $response->assertRedirect(route('tasks.index'));
        $this->assertDatabaseHas('tasks', ['id' => $task->id, 'name' => 'Updated Task', 'context' => 'Updated Context']
        );
    }
    public function test_cant_update_method_authenticated()
    {
        $task = Task::factory()->create();

        $response = $this->put(route('tasks.update', ['task' => $task->id]), [
            'name' => 'Updated Task',
            'context' => 'Updated Context',
        ]);

        $response->assertRedirect(route('login'));
    }
}
