<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    use  WithFaker;

    public function test_edit_method_returns_correct_view()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $task = Task::factory()->create();

        $response = $this->get(route('tasks.edit', ['task' => $task->id]));

        $response->assertViewIs('tasks.edit');
        $response->assertViewHas('task', $task);
    }
    public function test_edit_method_returns_error_for_unauthenticated_user()
    {
        $task = Task::factory()->create();

        $response = $this->get(route('tasks.edit', ['task' => $task->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
