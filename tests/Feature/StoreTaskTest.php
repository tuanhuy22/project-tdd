<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreTaskTest extends TestCase
{
    use  WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_store_method_creates_task_and_redirects()
    {
        // Log in a user
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->post(route('tasks.store'), [
            'name' => 'Test Task',
            'context' => 'Test Context',
        ]);

        $response->assertRedirect(route('tasks.index'));
        $this->assertDatabaseHas('tasks', ['name' => 'Test Task', 'context' => 'Test Context']);
    }
    public function test_cant_store_method_unauthenticated()
    {
        $response = $this->post(route('tasks.store'), [
            'name' => 'Test Task',
            'context' => 'Test Context',
        ]);

        $response->assertRedirect(route('login'));
    }
}
