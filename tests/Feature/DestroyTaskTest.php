<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DestroyTaskTest extends TestCase
{
    use  WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_destroy_method_deletes_task_and_redirects()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $task = Task::factory()->create();

        $response = $this->delete(route('tasks.destroy', ['task' => $task->id]));

        $response->assertRedirect(route('tasks.index'));
        $this->assertDatabaseMissing('tasks', ['id' => $task->id]);
    }
    public function test_cant_destroy_unathenticated()
    {
        $task = Task::factory()->create();

        $response = $this->delete(route('tasks.destroy', ['task' => $task->id]));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
