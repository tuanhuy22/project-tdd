function confirmDelete(taskId) {
    Swal.fire({
        title: 'Bạn có chắc không?',
        text: 'Bạn không thể hoàn tác ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        cancelButtonText: 'Hủy',
        confirmButtonText: 'Có, xóa task!'
    }).then((result) => {
        if (result.isConfirmed) {
            document.querySelector(`form[action='${window.location.origin}/tasks/${taskId}']`).submit();
        }
    });
}
